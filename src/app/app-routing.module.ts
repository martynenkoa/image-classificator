import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";

import { ClassifierComponent } from "./pages/classifier/components/classifier.component";
import { HomeComponent } from "./pages/home/components/home.component";

const routes: Routes = [
    {
        path: "home",
        component: HomeComponent,
    },
    {
        path: "classifier",
        component: ClassifierComponent,
    },
    {
        path: "**",
        redirectTo: "home",
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes),
    ],
    exports: [
        RouterModule
    ],
})
export class AppRoutingModule {
}
