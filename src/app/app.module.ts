import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";

import { ClassifierModule } from "./pages/classifier/classifier.module";
import { AppRoutingModule } from "./app-routing.module";
import { HomeModule } from "./pages/home/home.module";
import { AppComponent } from "./app.component";


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        ClassifierModule,
        FormsModule,
        HomeModule,
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
