import { animate, AnimationTriggerMetadata, state, style, transition, trigger } from "@angular/animations";

export class Animations {

    public static fadeIn(timings = 500)
    : AnimationTriggerMetadata {
        return trigger("fadeIn", [
            state("true", style({opacity: 1})),
            state("false", style({opacity: 0})),
            transition("0 => 1", animate(timings)),
            transition(":enter", [
                style({opacity: 0}),
                animate(timings, style({opacity: 1}))
            ]),
        ]);
    }
}
