export class DrawingCanvasDirectiveApi {

    public clear: () => void;
    public getCanvasElement: () => HTMLCanvasElement;

    constructor(init: DrawingCanvasDirectiveApi) {
        Object.assign(this, init);
    }
}
