export class MousePosition {

    public x: number;
    public y: number;

    constructor(init?: Partial<MousePosition>) {
        Object.assign(this, init);
    }
}
