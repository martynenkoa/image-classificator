import { Directive, ElementRef, EventEmitter, OnInit, Output } from "@angular/core";

import { MousePosition } from "./models/mouse-position.model";
import { DrawingCanvasDirectiveApi } from "./models/drawing-canvas-directive-api.model";

@Directive({
    selector: "[imgaDrawingCanvas]"
})
export class DrawingCanvasDirective implements OnInit {

    @Output()
    public directiveReady: EventEmitter<DrawingCanvasDirectiveApi>;

    private readonly canvas: HTMLCanvasElement;

    private mousePosition: MousePosition;
    private painting: boolean;

    constructor(
        private elementRef: ElementRef,
    ) {
        this.directiveReady = new EventEmitter<DrawingCanvasDirectiveApi>();

        this.mousePosition = new MousePosition();
        this.canvas = elementRef.nativeElement;
    }

    public ngOnInit()
    : void {
        this.subscribeOnEvents();

        this.onDrawingCanvasDirectiveReady();
    }

    private get api()
    : DrawingCanvasDirectiveApi {
        return new DrawingCanvasDirectiveApi({
            clear: this.clear.bind(this),
            getCanvasElement: () => this.canvas,
        })
    }

    private get canvasContext()
    : CanvasRenderingContext2D | null {
        if (!this.canvas || !this.canvas.getContext) {
            return null;
        }

        return this.canvas.getContext("2d");
    }

    private onMouseDown()
    : void {
        this.painting = true;

        this.canvasContext.beginPath();
    }

    public onMouseMove(event: MouseEvent)
    : void {
        if (!this.painting) {
            return;
        }

        this.mousePosition.x = event.offsetX;
        this.mousePosition.y = event.offsetY;

        this.canvasContext.lineWidth = 25;
        this.canvasContext.lineJoin = "round";
        this.canvasContext.lineCap = "round";
        this.canvasContext.strokeStyle = "#000000";

        this.paint();
    }

    public onMouseUp()
    : void {
        this.painting = false;
    }

    private clear()
    : void {
        this.canvasContext.clearRect(
            0,
            0,
            this.canvas.width,
            this.canvas.height
        );
    }

    private onDrawingCanvasDirectiveReady()
    : void {
        this.directiveReady.emit(this.api);
    }

    private paint()
    : void {
        this.canvasContext.lineTo(this.mousePosition.x, this.mousePosition.y);
        this.canvasContext.stroke();
    }

    private subscribeOnEvents()
    : void {
        this.canvas.addEventListener("mousedown", this.onMouseDown.bind(this));
        this.canvas.addEventListener("mousemove", this.onMouseMove.bind(this));
        this.canvas.addEventListener("mouseup", this.onMouseUp.bind(this));
    }
}

