import { NgModule } from "@angular/core";

import { DrawingCanvasDirective } from "./directives/drawing-canvas/drawing-canvas.directive";

@NgModule({
    declarations: [
        DrawingCanvasDirective,
    ],
    exports: [
        DrawingCanvasDirective,
    ],
})
export class SharedModule {

}
