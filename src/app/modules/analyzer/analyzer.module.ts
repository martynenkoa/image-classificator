import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AnalyzerViewComponent } from "./components/analyzer-view.component";
import { AnalyzerComponent } from "./containers/analyzer.component";

@NgModule({
    imports: [
        BrowserModule,
    ],
    declarations: [
        AnalyzerComponent,
        AnalyzerViewComponent,
    ],
    exports: [
        AnalyzerComponent,
    ],
})
export class AnalyzerModule {

}
