import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import * as mobilenet from "@tensorflow-models/mobilenet";

import { Prediction } from "../models/prediction.model";

@Component({
    selector: "imga-analyzer-view",
    templateUrl: "./analyzer-view.component.html",
    styleUrls: [ "./analyzer-view.component.scss" ]
})
export class AnalyzerViewComponent {

    @Input()
    public model: mobilenet.MobileNet;
    @Input()
    public predictions: Prediction[];

    @Output()
    public fileUpload: EventEmitter<HTMLImageElement>;

    @ViewChild("fileInput", { static: true })
    public fileInput: ElementRef<HTMLInputElement>;
    @ViewChild("imageElement", { static: false })
    public imageElement: ElementRef<HTMLImageElement>;

    public imageSrc: string | ArrayBuffer;

    constructor() {
        this.fileUpload = new EventEmitter<HTMLImageElement>();
    }

    public get fileName()
    : string {
        const fileInput = this.fileInput;

        if (getInputHasNoFilesInBuffer()) {
            return ;
        }

        return this.fileInput.nativeElement.files[0].name;

        // region Local Functions

        function getInputHasNoFilesInBuffer()
        : boolean {
            return !fileInput
                || !fileInput.nativeElement
                || !fileInput.nativeElement.files
                || !!fileInput.nativeElement.files[0];
        }

        // endregion
    }

    public onFileLoad(event: any)
    : void {
        if (getEventHasNoFilesInBuffer()) {
            return ;
        }

        this.updateImageSrc(event.target.files[0]);

        setTimeout(
            () => this.fileUpload.emit(
                this.imageElement.nativeElement
            ),
            100
        );

        // region Local Functions

        function getEventHasNoFilesInBuffer()
        : boolean {
            return !event || !event.target || !event.target.files;
        }

        // endregion
    }

    public openFileUpload()
    : void {
        this.fileInput.nativeElement.click();
    }

    private updateImageSrc(image: Blob)
    : void {
        const reader = new FileReader();
        reader.onload = () => this.imageSrc = reader.result;

        reader.readAsDataURL(image);
    }
}
