export class Prediction {

    public className: string;
    public probability: number;

    constructor(init: Prediction) {
        Object.assign(this, init);
    }
}
