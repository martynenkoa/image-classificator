import * as mobilenet from "@tensorflow-models/mobilenet";
import { Component, Input } from "@angular/core";

import { Prediction } from "../models/prediction.model";

@Component({
    selector: "imga-analyzer",
    templateUrl: "./analyzer.component.html",
})
export class AnalyzerComponent {

    @Input()
    public model: mobilenet.MobileNet;

    public predictions: Prediction[];

    public onFileUpload(imageElement: HTMLImageElement)
    : void {
        this.model
            .classify(imageElement)
            .then((predictions) =>
                this.predictions = predictions);
    }
}
