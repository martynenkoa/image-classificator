import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { HeaderComponent } from "./components/header.component";

@NgModule({
    imports: [
        RouterModule,
    ],
    declarations: [
        HeaderComponent,
    ],
    exports: [
        HeaderComponent,
    ],
})
export class HeaderModule {

}
