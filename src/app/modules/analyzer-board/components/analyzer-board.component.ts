import * as mobilenet from "@tensorflow-models/mobilenet";
import { Component } from "@angular/core";

import { CnnAnalyzerApi } from "../../cnn-analyzer/models/cnn-analyzer-api.model";

@Component({
    selector: "imga-analyzer-board",
    templateUrl: "./analyzer-board.component.html",
    styleUrls: [ "./analyzer-board.component.scss" ],
})
export class AnalyzerBoardComponent {

    public model: mobilenet.MobileNet;

    private cnnAnalyzerApi: CnnAnalyzerApi;

    constructor() {
        this.loadMobileNetModel();
    }

    public get isCnnModelTrained()
    : boolean {
        if (!this.cnnAnalyzerApi) {
            return false;
        }

        return this.cnnAnalyzerApi.getIsModelTrained();
    }

    public get cnnModelTrainingProgress()
    : number {
        if (!this.cnnAnalyzerApi || !this.cnnAnalyzerApi.getModelTrainingTimer()) {
            return 0;
        }

        return this.cnnAnalyzerApi.getModelTrainingTimer().progress;
    }

    public onCnnAnalyzerReady(api: CnnAnalyzerApi)
    : void {
        this.cnnAnalyzerApi = api;
    }

    private loadMobileNetModel()
    : void {
        mobilenet.load()
            .then((val) => this.model = val);
    }
}
