import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from "@angular/platform-browser";
import { MatTabsModule } from "@angular/material";
import { NgModule } from "@angular/core";

import { AnalyzerBoardComponent } from "./components/analyzer-board.component";
import { CnnAnalyzerModule } from "../cnn-analyzer/cnn-analyzer.module";
import { AnalyzerModule } from "../analyzer/analyzer.module";
import { LoaderModule } from "../loader/loader.module";

@NgModule({
    imports: [
        AnalyzerModule,
        BrowserAnimationsModule,
        BrowserModule,
        CnnAnalyzerModule,
        LoaderModule,
        MatTabsModule,
    ],
    declarations: [
        AnalyzerBoardComponent,
    ],
    exports: [
        AnalyzerBoardComponent,
    ],
})
export class AnalyzerBoardModule {

}
