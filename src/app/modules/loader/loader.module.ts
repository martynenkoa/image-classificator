import { MatProgressSpinnerModule } from "@angular/material";
import { NgModule } from "@angular/core";

import { LoaderComponent } from "./components/loader.component";

@NgModule({
    imports: [
        MatProgressSpinnerModule,
    ],
    declarations: [
        LoaderComponent,
    ],
    exports: [
        LoaderComponent,
    ],
})
export class LoaderModule {

}
