import { Component, Input } from "@angular/core";

@Component({
    selector: "imga-loader",
    templateUrl: "./loader.component.html",
    styleUrls: [ "./loader.component.scss", ],
})
export class LoaderComponent {

    @Input()
    public progress: number;

    public readonly loadingTitle = "Training model...";
    public readonly loadingText = "Sit back and take a sip of coffee \n while application trains the model";

    public get progressFormatted()
    : number {
        if (!this.progress) {
            return 0;
        }

        return this.progress * 100;
    }
}
