import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { CnnAnalyzerViewComponent } from "./components/cnn-analyzer-view.component";
import { DrawingCanvasModule } from "../draving-canvas/drawing-canvas.module";
import { CnnAnalyzerComponent } from "./containers/cnn-analyzer.component";

@NgModule({
    imports: [
        BrowserModule,
        DrawingCanvasModule,
        MatProgressSpinnerModule,
    ],
    declarations: [
	    CnnAnalyzerComponent,
	    CnnAnalyzerViewComponent,
    ],
    exports: [
	    CnnAnalyzerComponent,
    ],
})
export class CnnAnalyzerModule {

}
