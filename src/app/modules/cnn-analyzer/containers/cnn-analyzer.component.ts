import { Component, EventEmitter, OnInit, Output } from "@angular/core";

import { CnnAnalyzerHelperService } from "../components/services/cnn-analyzer-helper.service";
import { CnnAnalyzerService } from "../components/services/cnn-analyzer.service";
import { Prediction } from "../../analyzer/models/prediction.model";
import { SequentialModel } from "../models/sequential-model.model";
import { CnnAnalyzerApi } from "../models/cnn-analyzer-api.model";
import { MNISTData } from "../services/mnist-data.class";

@Component({
    selector: "imga-cnn-analyzer",
    templateUrl: "./cnn-analyzer.component.html",
    providers: [
        CnnAnalyzerHelperService,
        CnnAnalyzerService,
    ]
})
export class CnnAnalyzerComponent implements OnInit {

    @Output()
    public analyzerReady: EventEmitter<CnnAnalyzerApi>;

    public model: SequentialModel;
    public modelReady: boolean;
    public predictions: Prediction[];

    private dataset: MNISTData;

    constructor(
        private cnnAnalyzerHelperService: CnnAnalyzerHelperService,
        private cnnAnalyzerService: CnnAnalyzerService,
    ) {
        this.analyzerReady = new EventEmitter<CnnAnalyzerApi>();

        this.model = this.getModel();
        this.loadDataset();
    }

    public ngOnInit()
    : void {
        this.analyzerReady.emit(this.api);
    }

    private get api()
    : CnnAnalyzerApi {
        return new CnnAnalyzerApi({
            getIsModelTrained: () => this.modelReady,
            getModelTrainingTimer: () => this.cnnAnalyzerService.modelTrainingTimer,
        });
    }

    public doPrediction(canvasContext: CanvasRenderingContext2D)
    : void {
        const monoImgData = this.cnnAnalyzerHelperService.getMonoImageData(canvasContext);
        const predictionsList = this.cnnAnalyzerHelperService.getPredictionsList(monoImgData, this.model);

        const predictions = this.cnnAnalyzerHelperService.mapPredictionsArray(predictionsList);
        this.predictions = this.cnnAnalyzerHelperService.getTopThreePredictions(predictions);
    }

    public getModel()
    : SequentialModel {
        return this.cnnAnalyzerService.getCnnAnalyzerModel();
    }

    public loadDataset()
    : void {
        this.dataset = new MNISTData();

        this.dataset.load()
            .then(() => this.cnnAnalyzerService.trainCnnAnalyzerModel(this.model, this.dataset))
            .then(() => this.modelReady = true);
    }
}
