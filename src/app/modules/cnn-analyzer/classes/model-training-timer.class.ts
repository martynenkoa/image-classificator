import { BaseCallback } from "@tensorflow/tfjs-layers/src/base_callbacks";

import { ModelTrainingConfig } from "../models/model-training-config.model";

export class ModelTrainingTimer {

    private _callbacks: Partial<BaseCallback>[];
    private _progress: number;

    private periodLength: number;

    constructor(
        private trainingConfig: ModelTrainingConfig
    ) {
        this.initPeriods();
        this.initCallbacks();

        this._progress = 0;
    }

    public get callbacks()
    : Partial<BaseCallback>[] {
        return this._callbacks;
    }

    public get progress()
    : number {
        return this._progress;
    }

    private incrementProgress()
    : Promise<void> {
        this._progress += 1 / this.periodLength;
        return Promise.resolve();
    }

    private initCallbacks()
    : void {
        this._callbacks = [
            {
                onBatchEnd: () => this.incrementProgress(),
            }
        ];
    }

    private initPeriods()
    : void {
        this.periodLength = this.trainingConfig.trainDataSize / this.trainingConfig.batchSize * this.trainingConfig.epochs;
    }
}
