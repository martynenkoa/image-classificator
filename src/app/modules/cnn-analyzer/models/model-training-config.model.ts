export class ModelTrainingConfig {

    public batchSize: number;
    public epochs: number;
    public testDataSize: number;
    public trainDataSize: number;

    constructor(init: ModelTrainingConfig) {
        Object.assign(this, init);
    }
}
