import * as tf from "@tensorflow/tfjs";

export class ImageBatch {

    public xs: tf.Tensor<tf.Rank.R2>;
    public labels: tf.Tensor<tf.Rank.R2>;

    constructor(init: ImageBatch) {
        Object.assign(this, init);
    }
}
