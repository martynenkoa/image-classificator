import { ImageBatch } from "./image-batch.model";

export interface IData {

    load: () => Promise<void>;
    nextTrainBatch: (batchSize: number) => ImageBatch;
    nextTestBatch: (batchSize: number) => ImageBatch;
}
