import { ModelTrainingTimer } from "../classes/model-training-timer.class";

export class CnnAnalyzerApi {

    public getIsModelTrained: () => boolean;
    public getModelTrainingTimer: () => ModelTrainingTimer;

    constructor(init: CnnAnalyzerApi) {
        Object.assign(this, init);
    }
}
