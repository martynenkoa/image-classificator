export class BatchHandlerConfig {

    public batchSize: number;
    public data: (Float32Array | Uint8Array)[];
    public indexFn: () => number;

    constructor(init: BatchHandlerConfig) {
        Object.assign(this, init);
    }
}
