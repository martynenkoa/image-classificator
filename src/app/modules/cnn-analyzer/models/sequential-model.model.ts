import * as tf from "@tensorflow/tfjs";

export class SequentialModel extends tf.Sequential {

    constructor(init: SequentialModel) {
        super();
        Object.assign(this, init);
    }
}
