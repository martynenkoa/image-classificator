import { Component, EventEmitter, Input, Output } from "@angular/core";

import { DrawingCanvasApi } from "../../draving-canvas/models/drawing-canvas-api.model";
import { Prediction } from "../../analyzer/models/prediction.model";

@Component({
    selector: "imga-cnn-analyzer-view",
    styleUrls: [ "./cnn-analyzer-view.component.scss" ],
    templateUrl: "./cnn-analyzer-view.component.html",
})
export class CnnAnalyzerViewComponent {

	@Input()
	public modelReady: boolean;
	@Input()
	public predictions: Prediction[];

    @Output()
    public predict: EventEmitter<CanvasRenderingContext2D>;

    private drawingCanvasApi: DrawingCanvasApi;

    constructor() {
        this.predict = new EventEmitter<CanvasRenderingContext2D>();
    }

    public onClearCanvas()
    : void {
        this.drawingCanvasApi.clear();
    }

    public onDrawingCanvasApiReady(value: DrawingCanvasApi)
    : void {
        this.drawingCanvasApi = value;
    }

    private onPredict()
    : void {
        const context = this.drawingCanvasApi.getScaledCanvasContext();

        this.predict.emit(context);
    }
}
