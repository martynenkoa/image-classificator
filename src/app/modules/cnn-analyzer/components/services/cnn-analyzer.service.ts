import { Injectable } from "@angular/core";
import * as tf from "@tensorflow/tfjs";

import { MNISTData } from "../../services/mnist-data.class";
import { SequentialModel } from "../../models/sequential-model.model";
import { ModelTrainingConfig } from "../../models/model-training-config.model";
import { ModelTrainingTimer } from "../../classes/model-training-timer.class";

@Injectable()
export class CnnAnalyzerService {

    private readonly modelTrainingConfig: ModelTrainingConfig;
    private _modelTrainingTimer: ModelTrainingTimer;

    constructor() {
        this.modelTrainingConfig = {
            batchSize: 512,
            epochs: 10,
            trainDataSize: 5500,
            testDataSize: 1000,
        };
    }

    public get modelTrainingTimer()
    : ModelTrainingTimer {
        return this._modelTrainingTimer;
    }

    public getCnnAnalyzerModel()
    : SequentialModel {
        const model = tf.sequential();

        this.setCNNModelLayers(model);

        return model;
    }

    public async trainCnnAnalyzerModel(model: any, data: MNISTData)
    : Promise<any> {
        const [ trainXs, trainYs ] = this.getTrainTensors(data);
        const [ testXs, testYs ] = this.getTestTensors(data);

        this._modelTrainingTimer = new ModelTrainingTimer(this.modelTrainingConfig);

        return model.fit(
            trainXs,
            trainYs,
            {
                batchSize: this.modelTrainingConfig.batchSize,
                validationData: [testXs, testYs],
                epochs: this.modelTrainingConfig.epochs,
                shuffle: true,
                callbacks: this._modelTrainingTimer.callbacks,
            }
        );
    }

    private get testReshapeConfig()
    : number[] {
        return [this.modelTrainingConfig.testDataSize, 28, 28, 1];
    }

    private get trainReshapeConfig()
    : number[] {
        return [this.modelTrainingConfig.trainDataSize, 28, 28, 1];
    }

    private getTestTensors(data: MNISTData)
    : tf.Tensor<tf.Rank>[] {
        return tf.tidy(() => {

            const d = data.nextTestBatch(this.modelTrainingConfig.testDataSize);
            return [
                d.xs.reshape(this.testReshapeConfig),
                d.labels
            ];
        });
    }

    private getTrainTensors(data: MNISTData)
    : tf.Tensor<tf.Rank>[] {
        return tf.tidy(() => {
            const d = data.nextTrainBatch(this.modelTrainingConfig.trainDataSize);
            return [
                d.xs.reshape(this.trainReshapeConfig),
                d.labels
            ];
        });
    }

    private setCNNModelLayers(model: SequentialModel)
    : void {
        const layers = [
            tf.layers.conv2d(getFirstConvolutionalLayerConfig()),
            tf.layers.maxPooling2d(getFirstPoolingLayerConfig()),
            tf.layers.conv2d(getSecondConvolutionalLayerConfig()),
            tf.layers.maxPooling2d(getSecondPoolingLayerConfig()),
            tf.layers.flatten(),
            tf.layers.dense(getDenseLayerConfig()),
        ];

        layers.forEach(layer => model.add(layer));

        model.compile(getModelCompileConfig());

        // region Local Functions

        function getFirstConvolutionalLayerConfig()
        : any {
            return {
                inputShape: [28, 28, 1],
                kernelSize: 5,
                filters: 8,
                strides: 1,
                activation: "relu",
                kernelInitializer: "varianceScaling"
            };
        }

        function getFirstPoolingLayerConfig()
        : any {
            return {
                poolSize: [2, 2],
                strides: [2, 2]
            };
        }

        function getSecondConvolutionalLayerConfig()
        : any {
            return {
                kernelSize: 5,
                filters: 16,
                strides: 1,
                activation: "relu",
                kernelInitializer: "varianceScaling"
            };
        }

        function getSecondPoolingLayerConfig()
        : any {
            return {
                poolSize: [2, 2],
                strides: [2, 2]
            };
        }

        function getDenseLayerConfig()
        : any {
            return {
                units: 10,
                kernelInitializer: "varianceScaling",
                activation: "softmax"
            };
        }

        function getModelCompileConfig()
        : any {
            return {
                optimizer: tf.train.adam(),
                loss: "categoricalCrossentropy",
                metrics: ["accuracy"],
            };
        }

        // endregion
    }
}
