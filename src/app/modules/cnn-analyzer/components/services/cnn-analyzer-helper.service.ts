import { Injectable } from "@angular/core";
import * as tf from "@tensorflow/tfjs";
import * as _ from "lodash";

import { SequentialModel } from "../../models/sequential-model.model";
import { Prediction } from "../../../analyzer/models/prediction.model";

@Injectable()
export class CnnAnalyzerHelperService {

    public getMonoImageData(canvasContext: CanvasRenderingContext2D)
    : ImageData {
        const imgdata = canvasContext.getImageData(0, 0, 28, 28);

        const monodata = [];

        for (let i = 0, len = imgdata.data.length / 4; i < len; i += 1) {
            monodata.push(imgdata.data[i * 4 + 3]);
            monodata.push(0);
            monodata.push(0);
            monodata.push(0);
        }

        return new ImageData(
            new Uint8ClampedArray(monodata),
            28,
            28
        );
    }

    public getPredictionsList(imageData: ImageData, model: SequentialModel)
    : number[] {
        const input = tf.browser
            .fromPixels(imageData, 1)
            .reshape([1, 28, 28, 1])
            .cast("float32")
            .div(tf.scalar(255));

        const predicts = <number[]>(model as any).predict(input).dataSync();

        return Array.from(predicts);
    }

    public getTopThreePredictions(predictions: Prediction[])
    : Prediction[] {
        const sortedPredictions = _.orderBy(predictions, "probability", "desc");

        return sortedPredictions.slice(0, 3);
    }

    public mapPredictionsArray(values: number[])
    : Prediction[] {
        return values.map((prediction) =>
            new Prediction({
                className: values.indexOf(prediction).toString(),
                probability: _.round(prediction, 2)
            }));
    }
}


