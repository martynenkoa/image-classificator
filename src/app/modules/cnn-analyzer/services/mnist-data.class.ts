import * as tf from "@tensorflow/tfjs";

import { BatchHandlerConfig } from "../models/batch-handler-config.model";
import { ImageBatch } from "../models/image-batch.model";
import { IData } from "../models/data.interface";

export class MNISTData implements IData {

    private readonly trainIndex = 5 / 6;

    private readonly datasetElementsAmount = 65000;
    private readonly classesAmount = 10;
    private readonly imageSize = 784;

    private readonly trainElementsAmount = Math.floor(this.trainIndex * this.datasetElementsAmount);
    private readonly testElementsAmount = this.datasetElementsAmount - this.trainElementsAmount;

    private readonly imagesSpriteSrc = "https://storage.googleapis.com/learnjs-data/model-builder/mnist_images.png";
    private readonly labelsPath = "https://storage.googleapis.com/learnjs-data/model-builder/mnist_labels_uint8";

    private shuffledTrainIndex: number;
    private shuffledTestIndex: number;

    private datasetImages: Float32Array;
    private datasetLabels: Uint8Array;
    private trainIndexes: Uint32Array;
    private testIndexes: Uint32Array;
    private trainImages: Float32Array;
    private testImages: Float32Array;
    private trainLabels: Uint8Array;
    private testLabels: Uint8Array;

    constructor() {
        this.shuffledTrainIndex = 0;
        this.shuffledTestIndex = 0;
    }

    public async load()
    : Promise<void> {
        const img = new Image();
        const canvas = document.createElement("canvas");
        const ctx = canvas.getContext("2d");

        const imgRequest = new Promise(resolve => {

            img.crossOrigin = "";
            img.onload = () => {

                img.width = img.naturalWidth;
                img.height = img.naturalHeight;

                const datasetBytesBuffer =
                    new ArrayBuffer(this.datasetElementsAmount * this.imageSize * 4);

                const chunkSize = 5000;

                canvas.width = img.width;
                canvas.height = chunkSize;

                for (let i = 0; i < this.datasetElementsAmount / chunkSize; i++) {
                    const datasetBytesView = new Float32Array(
                        datasetBytesBuffer,
                        i * this.imageSize * chunkSize * 4,
                        this.imageSize * chunkSize);

                    ctx.drawImage(img, 0, i * chunkSize, img.width, chunkSize, 0, 0, img.width, chunkSize);

                    const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);

                    for (let j = 0; j < imageData.data.length / 4; j++) {

                        datasetBytesView[j] = imageData.data[ j * 4 ] / 255;
                    }
                }

                this.datasetImages = new Float32Array(datasetBytesBuffer);

                resolve();
            };

            img.src = this.imagesSpriteSrc;
        });

        const labelsRequest = fetch(this.labelsPath);
        const [ imgResponse, labelsResponse ] = await Promise.all([imgRequest, labelsRequest]);

        this.datasetLabels = new Uint8Array(await labelsResponse.arrayBuffer());

        this.trainIndexes = tf.util.createShuffledIndices(this.trainElementsAmount);
        this.testIndexes = tf.util.createShuffledIndices(this.testElementsAmount);

        this.trainImages = this.datasetImages.slice(0, this.imageSize * this.trainElementsAmount);
        this.testImages = this.datasetImages.slice(this.imageSize * this.trainElementsAmount);

        this.trainLabels = this.datasetLabels.slice(0, this.classesAmount * this.trainElementsAmount);
        this.testLabels = this.datasetLabels.slice(this.classesAmount * this.trainElementsAmount);
    }

    public nextTrainBatch(batchSize: number)
    : ImageBatch {
        const config = new BatchHandlerConfig({
            data: [ this.trainImages, this.trainLabels ],
            indexFn: () => {
                this.shuffledTrainIndex = (this.shuffledTrainIndex + 1) % this.trainIndexes.length;
                return this.trainIndexes[this.shuffledTrainIndex];
            },
            batchSize
        });

        return this.nextBatch(config);
    }

    public nextTestBatch(batchSize: number)
    : ImageBatch {
        const config = new BatchHandlerConfig({
            data: [ this.testImages, this.testLabels ],
            indexFn: () => {
                this.shuffledTestIndex = (this.shuffledTestIndex + 1) % this.testIndexes.length;
                return this.testIndexes[this.shuffledTestIndex];
            },
            batchSize
        });

        return this.nextBatch(config);
    }

    private nextBatch(config: BatchHandlerConfig)
    : ImageBatch {
        const  { batchSize, indexFn, data } = config;

        const batchImagesArray = new Float32Array(batchSize * this.imageSize);
        const batchLabelsArray = new Uint8Array(batchSize * this.classesAmount);

        for (let i = 0; i < batchSize; i++) {
            const index = indexFn();

            const image = data[0].slice(index * this.imageSize, index * this.imageSize + this.imageSize);
            batchImagesArray.set(image, i * this.imageSize);

            const label = data[1].slice(index * this.classesAmount, index * this.classesAmount + this.classesAmount);
            batchLabelsArray.set(label, i * this.classesAmount);
        }

        const xs = tf.tensor2d(batchImagesArray, [batchSize, this.imageSize]);
        const labels = tf.tensor2d(batchLabelsArray, [batchSize, this.classesAmount]);

        return new ImageBatch({ xs, labels });
    }
}
