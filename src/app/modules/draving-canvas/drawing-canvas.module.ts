import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { DrawingCanvasComponent } from "./components/drawing-canvas.component";
import { SharedModule } from "../../shared/shared.module";

@NgModule({
    imports: [
        BrowserModule,
        SharedModule,
    ],
    declarations: [
        DrawingCanvasComponent,
    ],
    exports: [
        DrawingCanvasComponent,
    ],
})
export class DrawingCanvasModule {

}
