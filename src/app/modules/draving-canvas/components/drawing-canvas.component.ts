import { Component, EventEmitter, Output } from "@angular/core";

import { DrawingCanvasDirectiveApi } from "../../../shared/directives/drawing-canvas/models/drawing-canvas-directive-api.model";
import { DrawingCanvasApi } from "../models/drawing-canvas-api.model";

@Component({
    selector: "imga-drawing-canvas",
    styleUrls: [ "./drawing-canvas.component.scss" ],
    templateUrl: "./drawing-canvas.component.html",
})
export class DrawingCanvasComponent {

    @Output()
    public drawingCanvasApiReady: EventEmitter<DrawingCanvasApi>;

    public readonly canvasHeight = 400;
    public readonly canvasWidth = 400;
    public readonly datasetImageHeight = 28;
    public readonly datasetImageWidth = 28;

    private drawingCanvasDirectiveApi: DrawingCanvasDirectiveApi;

    constructor() {
        this.drawingCanvasApiReady = new EventEmitter<DrawingCanvasApi>();
    }

    private get api()
    : DrawingCanvasApi {
        return new DrawingCanvasApi({
            clear: this.drawingCanvasDirectiveApi.clear.bind(this),
            getScaledCanvasContext: this.getScaledCanvasContext.bind(this),
        });
    }

    public onDrawingCanvasDirectiveReady(api: DrawingCanvasDirectiveApi)
    : void {
        this.drawingCanvasDirectiveApi = api;

        this.onDrawingCanvasReady();
    }

    private createScaledCanvas()
    : HTMLCanvasElement {
        const newCanvas = document.createElement("canvas");

        newCanvas.height = this.datasetImageHeight;
        newCanvas.width = this.datasetImageWidth;

        newCanvas
            .getContext("2d")
            .drawImage(
                this.drawingCanvasDirectiveApi.getCanvasElement(),
                0,
                0,
                this.canvasWidth,
                this.canvasHeight,
                0,
                0,
                this.datasetImageWidth,
                this.datasetImageHeight
            );

        return newCanvas;
    }

    private getScaledCanvasContext()
    : CanvasRenderingContext2D {
        const scaledCanvas = this.createScaledCanvas();

        return scaledCanvas.getContext("2d");
    }

    private onDrawingCanvasReady()
    : void {
        this.drawingCanvasApiReady.emit(this.api);
    }
}
