export class DrawingCanvasApi {

    public clear: Function;
    public getScaledCanvasContext: () => CanvasRenderingContext2D;

    constructor(init: DrawingCanvasApi) {
        Object.assign(this, init);
    }
}
