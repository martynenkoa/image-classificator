import { Component } from "@angular/core";

import { Animations } from "../../../shared/animations/animations";

@Component({
    selector: "imga-classifier",
    templateUrl: "./classifier.component.html",
    animations: [ Animations.fadeIn() ],
})
export class ClassifierComponent {

}
