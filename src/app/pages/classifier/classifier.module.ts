import { NgModule } from "@angular/core";

import { AnalyzerBoardModule } from "../../modules/analyzer-board/analyzer-board.module";
import { CnnAnalyzerModule } from "../../modules/cnn-analyzer/cnn-analyzer.module";
import { ClassifierComponent } from "./components/classifier.component";
import { HeaderModule } from "../../modules/header/header.module";
import { LoaderModule } from "../../modules/loader/loader.module";

@NgModule({
    imports: [
        AnalyzerBoardModule,
        CnnAnalyzerModule,
        HeaderModule,
        LoaderModule,
    ],
    declarations: [
        ClassifierComponent,
    ],
})
export class ClassifierModule {

}
