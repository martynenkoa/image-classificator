import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { HomeComponent } from "./components/home.component";

@NgModule({
    imports: [
        RouterModule,
    ],
    declarations: [
        HomeComponent,
    ],
})
export class HomeModule {

}
