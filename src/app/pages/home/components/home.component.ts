import { Component } from "@angular/core";

import { Animations } from "../../../shared/animations/animations";

@Component({
    selector: "imga-home",
    templateUrl: "./home.component.html",
    styleUrls: [ "./home.component.scss" ],
    animations: [ Animations.fadeIn() ],
})
export class HomeComponent {

}
